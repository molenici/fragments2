package com.example.molenici.fragments2.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.molenici.fragments2.R;

public class ItemViewHolder extends RecyclerView.ViewHolder {

    public TextView idTextView;
    public TextView contentTextView;

    public ItemViewHolder(View itemView) {
        super(itemView);
        idTextView = itemView.findViewById(R.id.id);
        contentTextView = itemView.findViewById(R.id.content);
    }
}
