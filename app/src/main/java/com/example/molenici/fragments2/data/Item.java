package com.example.molenici.fragments2.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {

    private final int id;
    private final String content;
    private final String details;

    public Item(int id, String content, String details) {
        this.id = id;
        this.content = content;
        this.details = details;
    }

    protected Item(Parcel in) {
        id = in.readInt();
        content = in.readString();
        details = in.readString();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getDetails() {
        return details;
    }

    @Override
    public String toString() {
        return content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(content);
        dest.writeString(details);
    }

}
