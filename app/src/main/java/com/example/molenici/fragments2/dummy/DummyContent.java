package com.example.molenici.fragments2.dummy;

import com.example.molenici.fragments2.data.Item;

import java.util.ArrayList;
import java.util.List;

public class DummyContent {

    public static final List<Item> ITEMS = new ArrayList<>();

    private static final int COUNT = 25;

    static {
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(Item item) {
        ITEMS.add(item);
    }

    private static Item createDummyItem(int position) {
        return new Item(position, "Item content " + position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        return "Details about item: " + position;
    }
}