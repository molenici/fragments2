package com.example.molenici.fragments2.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.molenici.fragments2.R;
import com.example.molenici.fragments2.data.Item;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private List<Item> items;

    public ItemAdapter(List<Item> items) {
        this.items = items;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item, parent, false);
        return new ItemViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.idTextView.setText(String.valueOf(items.get(position).getId()));
        holder.contentTextView.setText(items.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
