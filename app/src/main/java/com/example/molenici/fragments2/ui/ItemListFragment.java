package com.example.molenici.fragments2.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.molenici.fragments2.R;
import com.example.molenici.fragments2.adapters.ItemAdapter;
import com.example.molenici.fragments2.data.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemListFragment extends Fragment {

    private List<Item> mItems = new ArrayList<>();

    public static ItemListFragment newInstance() {
        Bundle args = new Bundle();
        ItemListFragment itemListFragment = new ItemListFragment();
        itemListFragment.setArguments(args);
        return itemListFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_list);
        recyclerView.setAdapter(new ItemAdapter(mItems));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

}
